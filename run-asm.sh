#! /bin/bash
name=$1
if [[ $name == "" ]]; then
  name="hello"
fi

echo "compiling..."
as -o $name".o" $name".asm"
ld -o $name $name".o"
echo executing...
./$name
